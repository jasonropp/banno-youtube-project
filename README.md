##Currently Working On

1. Reorganizing the flow of the app so that the top level of video content is the video-search with a direct line to video-display

search manager / comments manager / favorites manager / stats manager

then search display / comments disp / favorites display /stats display

all managers and display then connect back into video display for their respective pieces to show up 


##Stuck
 
1. For some reason on load the favorites adds a bunch of remove buttons


##Functions for the service

1. Function that takes custom url to make ajax request for comments, video search, etc.
2. A local storage function that takes arguments for the local storage location and what goes into it






# Banno Youtube Player Project

A video app that allows the user to search by keyword and display the clicked video as well as save the video into a list of favorites. The favorites list is stored in local storage and avaialable between browsing sessions unless browser history is deleted. 

##Sensitive Data

The repository for the code is set to private, the youtube Data API key for this project is exposed. While the key is not of a sensitive nature, precautions were still taken.

##How To Use

###Opening the App (and a bug). 

The app opens best when opened in an Incognito Window.

Opening the app in a regular browser returns a 404 for reasons I have not yet figured out. In the event of a 404, just click the Banno Video Project and the app will display.

###Video Search

Enter a video in the search bar and click submit. A list of search results will be displayed. Click one to display the video and it's likes, dislikes, veiws, channel name and title of the video.

###Favorite Video

Clicking the "favorite" button below the video player. The results will be added into the favorites section. Click a favorite video to re-display it.

###Local Storage

The favorited videos will be available between browsing sessions, even if you close the browser. If you reset your browser history, the videos will no longer be saved.

###Filter Results

The dropdown box below search results sorts the videos by their rating, date, or relevance. 
